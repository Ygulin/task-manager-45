package ru.tsc.gulin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataJsonJaxbSaveRequest extends AbstractUserRequest {

    public DataJsonJaxbSaveRequest(@Nullable final String token) {
        super(token);
    }

}
