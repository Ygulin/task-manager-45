package ru.tsc.gulin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.gulin.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.gulin.tm.command.AbstractCommand;
import ru.tsc.gulin.tm.enumerated.Role;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gulin.tm.dto.model.TaskDTO;
import ru.tsc.gulin.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return serviceLocator.getTaskEndpoint();
    }

    @NotNull
    public IProjectTaskEndpoint getProjectTaskEndpoint() {
        return serviceLocator.getProjectTaskEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(@Nullable final List<TaskDTO> tasks) {
        int index = 1;
        for (@Nullable final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("START DATE: " + DateUtil.toString(task.getDateStart()));
        System.out.println("END DATE: " + DateUtil.toString(task.getDateEnd()));
    }

}
