package ru.tsc.gulin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "help";

    @NotNull
    public static final String DESCRIPTION = "Display list of terminal commands";

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final AbstractCommand command : commands) System.out.println(command);
    }

}
