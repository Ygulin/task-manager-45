package ru.tsc.gulin.tm.api.service.model;

import ru.tsc.gulin.tm.api.repository.model.IRepository;
import ru.tsc.gulin.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
