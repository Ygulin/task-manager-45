package ru.tsc.gulin.tm.api.repository.dto;

import ru.tsc.gulin.tm.dto.model.ProjectDTO;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {
}
