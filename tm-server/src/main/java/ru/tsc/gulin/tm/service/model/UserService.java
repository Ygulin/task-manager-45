package ru.tsc.gulin.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.model.IUserRepository;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.IPropertyService;
import ru.tsc.gulin.tm.api.service.model.IUserService;
import ru.tsc.gulin.tm.enumerated.Role;
import ru.tsc.gulin.tm.exception.field.*;
import ru.tsc.gulin.tm.exception.system.AccessDeniedException;
import ru.tsc.gulin.tm.model.User;
import ru.tsc.gulin.tm.repository.model.UserRepository;
import ru.tsc.gulin.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    protected @NotNull UserRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final UserRepository repository = getRepository(entityManager);
            return repository.findOneByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findOneByLogin(login) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOneByEmail(@Nullable final String email) {
        Optional.ofNullable(email).filter(item -> !item.isEmpty()).orElseThrow(EmailEmptyException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final UserRepository repository = getRepository(entityManager);
            return repository.findOneByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findOneByEmail(email) != null;
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findOneByLogin(login);
        remove(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        if (isLoginExists(login)) throw new LoginExistsException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final User user = new User();
        try {
            @NotNull final UserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(Role.USUAL);
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email).filter(item -> !item.isEmpty()).orElseThrow(EmailEmptyException::new);
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable final User user = new User();
        try {
            @NotNull final UserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setEmail(email);
            user.setRole(Role.USUAL);
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(role).orElseThrow(RoleEmptyException::new);
        if (isLoginExists(login)) throw new LoginExistsException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable final User user = new User();
        try {
            @NotNull final UserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(role);
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        @Nullable final User user = findOneById(userId);
        if (user == null) return null;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        update(user);
        return user;
    }

    @Nullable
    @Override
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        @Nullable final User user = findOneById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @Nullable final User user= findOneByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        update(user);
    }

}
