package ru.tsc.gulin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.gulin.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.Date;

public interface IUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO> extends IUserOwnedRepositoryDTO<M>, IServiceDTO<M> {

}
